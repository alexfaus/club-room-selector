//
// Created by alexfaus on 12/7/17.
//

#include <sstream>
#include <string>
#include <iostream>
#include <array>
#include "Room.h"

using namespace std;


// Constructor for empty Room
Room::Room()
{
    array<string, SIZE> dates;
    for (int i = 0; i < SIZE; i++)
    {
        dates[i] = "";
    }
    this->name = "";
    this->cap = 0;
    this->tables = 0;
    this->wb = NULL;
    this->tech = NULL;
    this->description = "";
    this->display = "";
    this->gradeValue = 0.0;
}

// Constructor for Room with given Variables
Room::Room(string name,  array<string, SIZE> dates, int cap, int tables , bool wb, bool tech, string description)
{
    this->name = name;
    this->dates = dates;
    this->cap = cap;
    this->tables = tables;
    this->wb = wb;
    this->tech = tech;
    this->description = description;

    // display and gradeValue get updated during grade process
    this->display = "";
    this->gradeValue = 0.0;
}

// Getters
string Room::getName()
{
    return this->name;
}

array<string, SIZE> Room::getDates()
{
    return this->dates;
}

int Room::getCap()
{
    return this->cap;
}

int Room::getTables()
{
    return this->tables;
}

bool Room::getWb()
{
    return this->wb;
}

bool Room::getTech()
{
    return this->tech;
}

string Room::getDescription()
{
    return this->description;
}

string Room::getDisplay()
{
    return this->display;
}

float Room::getGradeValue()
{
    return this->gradeValue;
}

// Setters
void Room::setName(string name)
{
    this->name = name;
}

void Room::setDates(array<string, SIZE> dates)
{
    this->dates = dates;
}

void Room::setCap(int cap)
{
    this->cap = cap;
}

void Room::setTables(int tables)
{
    this->tables = tables;
}

void Room::setWb(bool wb)
{
    this->wb = wb;
}

void Room::setTech(bool tech)
{
    this->tech = tech;
}

void Room::setDescription(string description)
{
    this->description = description;
}

void Room::setDisplay(string display)
{
    this->display = display;
}

void Room::setGradeValue(float gradeValue)
{
    this->gradeValue = gradeValue;
}

// The Grade Function: takes a parameter of a Default Room from the RoomList. Returns a float value of a grade.
float Room::grade(Room &DefaultRoom)
{
    float grade = 0.0;
    string date_of_event = this->dates[0];

    // Check Dates
    for (int x  = 0; x < SIZE; x++)
    {
        if (date_of_event == DefaultRoom.dates[x])
        {
            grade++;
            DefaultRoom.display.append("Has the available date: " + date_of_event + "\n");
        }
        if (DefaultRoom.dates[x] == "")
        {
            // Reached point in array where no more dates exist
            break;
        }
    }

    // Check Capacity
    if (this->cap <= DefaultRoom.cap)
    {
        grade++;
        DefaultRoom.display.append("Has enough capacity: " + to_string(DefaultRoom.getCap()) + "\n");
    }


    // Check Tables
    if (this->tables <= DefaultRoom.tables)
    {
        grade++;
        DefaultRoom.display.append("Has enough tables: " + to_string(DefaultRoom.getTables()) + "\n");
    }

    // Check Whiteboard
    if (this->wb == DefaultRoom.wb)
    {
        grade++;
        DefaultRoom.display.append("Matches Whiteboard Request \n");
    }

    // Check Tech
    if (this->tech == DefaultRoom.tech)
    {
        grade++;
        DefaultRoom.display.append("Matches Technology Request \n");
    }

    return grade;
}




Room::~Room()
{

}

//bool operator < (const Room &room1, const Room &room2)
//{
//    if (room1.gradeValue < room2.gradeValue)
//    {
//        return true;
//    }
//    else
//    {
//        return false;
//    }
//}
