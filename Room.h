//
// Created by alexfaus on 12/7/17.
//

#ifndef ALG_PROJECT_ROOM_H
#define ALG_PROJECT_ROOM_H

#include <sstream>
#include <string>
#include <iostream>
#include <array>

using namespace std;

const int SIZE = 20;


class Room {
public:
    float gradeValue;
    Room(string name, array<string, SIZE> dates, int cap, int tables , bool wb, bool tech, string description); // constructor
    Room();
    ~Room(); // deconstructor

    // Class Methods
    float grade(Room &DefaultRoom);
    friend bool operator<(const Room &room1, const Room &room2);

    // getters
    string getName();
    array<string, SIZE> getDates();
    int getCap();
    int getTables();
    bool getWb();
    bool getTech();
    string getDescription();
    string getDisplay();
    float getGradeValue();

    // setters
    void setName(string name);
    void setDates( array<string, SIZE> dates);
    void setCap(int cap);
    void setTables(int tables);
    void setWb(bool wb);
    void setTech(bool tech);
    void setDescription(string description);
    void setDisplay(string display);
    void setGradeValue(float gradeValue);

private:
    // Int Capacity, Boolean Tech Features, Boolean Whiteboard, Int Tables, String "style"
    string name;
    array<string, SIZE> dates;
    int cap;
    int tables;
    bool wb;
    bool tech;
    string description;
    string display;

};



#endif //ALG_PROJECT_ROOM_H
