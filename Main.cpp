// Alexander Faus, Jess Moser, and Kara Konieczny

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include "Room.h"
#include <array>
#include <algorithm>

using namespace std;

const int ROOMSONCAMPUS = 10;


// Used for the bubble sort
bool operator < (const Room &room1, const Room &room2)
{
    // Sort the Rooms by grade values
    if (room1.gradeValue < room2.gradeValue)
    {
        return true;
    }
    else
    {
        return false;
    }
}


// Bubble sort but for rooms
// Unfortunately had difficulty using more efficient sorting algorithms
void bubbleSort(array<Room, ROOMSONCAMPUS> &roomArray, int n)
{
    int i, j;
    for (i = 0; i < n-1; i++)

        // Last i elements are already in place
        for (j = 0; j < n-i-1; j++) {
            if (roomArray[j] < roomArray[j + 1])
            {
                Room tempRoom = Room();
                tempRoom = roomArray[j];
                roomArray[j] = roomArray[j + 1];
                roomArray[j + 1] = tempRoom;
            }
        }
}


// Function used to create an instance of the Room class from a text file
void parseText(array<Room, ROOMSONCAMPUS> &RoomList)
{
    // Parse Text into variables
    // Use variables to create new room
    // Add Room to array
    // Iterate to next room in text file

    // NOTE TO DR MATHIAS: May need to change this line of code. It did not work when we did not have the full
    // location of the text file.
    ifstream in("/Users/Alex/College/Fall 2017/Git Repo for Alg Project/alg-project/roomlist.txt");

    string str;

    if(!in) {
        cout << "Cannot open input file.\n";
    }

    bool notEndFile = true;

    getline(in, str);
    int j = 0;

    while (notEndFile) {

        // 7 Lines for each room
        // Line 1: Name of Room
        // Line 2: Dates available
        // Line 3: Capacity
        // Line 4: Tables
        // Line 5: Whiteboard
        // Line 6: Tech
        // Line 7 - ?: Description

        string name;
        array<string, SIZE> dates;
        int cap;
        int tables;
        bool wb;
        bool tech;
        string description;

        for (int x = 1; x <= 7; x++)
        {
            // the x value is which line per room we are on
            if (x == 1)
            {
                name = str;

            }

            else if (x == 2)
            {
                istringstream iss(str);
                int index = 0;
                do
                {
                    string subs;
                    iss >> subs;
                    dates[index] = subs;
                    index++;
                } while (iss);
            }

            else if (x == 3)
            {
                istringstream ( str ) >> cap;
            }

            else if (x == 4)
            {
                istringstream ( str ) >> tables;
            }

            else if (x == 5)
            {
                if(str == "Y")
                {
                    wb = true;
                }
                else
                {
                    wb = false;
                }
            }
            else if (x == 6)
            {
                if(str == "Y")
                {
                    tech = true;
                }
                else
                {
                    tech = false;
                }
            }
            else if (x == 7)
            {
                do
                {
                    description += str;
                    getline(in, str);
                }while(!(str == "ENDROOM"));
            }
            getline(in, str);
        }
        // Take the variables parsed and create a room
        RoomList[j] = Room(name, dates, cap, tables, wb, tech, description);
        j++;
        // j is which room we are on in RoomList so increment j for next room

        // if not at the end of the file, move on to the next line for next room
        if (str == "ENDFILE")
        {
            notEndFile = false;
        }

    }
    // close file
    in.close();
}

// Prompter for User to enter details about their event
Room UserInput()
{
    // clear out cin because we use getline method for strings with spaces
    cin.ignore();
    string name;
    string date;
    array<string, SIZE> dates;
    string capString;
    int cap;
    int tables;
    // certain variables have strings to convert to respected values later
    string tablesString;
    bool wb;
    string wbString;
    bool tech;
    string techString;
    string description;

    cout << "Name of your event: ";
    getline(cin, name);
    cout << "Enter the date of your event in the format MM/DD/YYYY ";
    cin >> date;
    cout << "Number of people for your event: ";
    cin >> capString;
    istringstream ( capString ) >> cap;
    cout << "Number of tables needed for your event: ";
    cin >> tablesString;
    istringstream ( tablesString ) >> tables;
    cout << "Do you need a whiteboard? (Y / N): ";
    cin >> wbString;
    if(wbString == "Y")
    {
        wb = true;
    }
    else
    {
        wb = false;
    }
    cout << "Do you need technology? (Y / N): ";
    cin >> techString;
    if(techString == "Y")
    {
        tech = true;
    }
    else
    {
        tech = false;
    }
    cin.ignore();
    cout << "Please enter a description of your event. Include specifics over technology needed if applicable." << endl;
    getline(cin, description);
    // load the date the user gave into the dates array (which is empty hence index 0)
    dates[0] = date;
    Room UserEvent(name, dates, cap, tables , wb, tech, description);
    // return the room to main
    return UserEvent;
}

int main() {
    string UserName;
    string UserClub;
    string UserEmail;
    int end = 0;
    array<Room, ROOMSONCAMPUS> RoomList;
    // takes the roomlist.txt file and creates an array of rooms (RoomList)
    parseText(RoomList);

    cout << "Welcome" << endl;
    cout << "Please enter your name: ";
    getline(cin, UserName);
    cout << "Please enter the name of your club: ";
    getline(cin, UserClub);
    cout << "Please enter your email: ";
    cin >> UserEmail;

    // The user event is stored as a "Room" variable for simplicity
    Room UserEvent = UserInput();

    // for each room in the room list
    for (int x = 0; x <= ROOMSONCAMPUS; x++)
    {
        // if we reach a room with an empty name, all the rest of the Rooms in the RoomList array are filler rooms
        // therefore, break the for loop
        if(RoomList[x].getName() == "")
        {
            end = x;
            break;
        }
        // Grade the room from RoomList and update its gradeValue variable
        // this method is passed by reference so it also updates the Room's display which is used later on
        // when the rooms are displayed and the "How this room compares to your event: "
        RoomList[x].setGradeValue(UserEvent.grade(RoomList[x]));
    }

    // sort RoomList by each Room's gradeValue
    bubbleSort(RoomList, end);

    // pretty formatting
    cout << endl << endl;

    // For each room in RoomList print the room description and its display (how to compares to UserEvent)
    for (int y = 0; y <= ROOMSONCAMPUS; y++)
    {
        // once again, avoid comparing the filler rooms
        if(RoomList[y].getName() == "")
        {
            break;
        }


        cout << "Name of Room: " << RoomList[y].getName() << endl;
        cout << "Dates available: ";
        // iterate through the array of dates (which are strings)
        for (int x  = 0; x < SIZE; x++)
        {
            cout << RoomList[y].getDates()[x] + " ";
        }
        cout << endl;
        cout << "Room Capacity: " << RoomList[y].getCap() << endl;
        cout << "Number of Tables: " << RoomList[y].getTables() << endl;
        cout << "Has Whiteboard?: ";
        if(RoomList[y].getWb())
        {
            cout << "Yes" << endl;
        }
        else
        {
            cout << "No" << endl;
        }
        cout << "Has Technology to Use?: ";
        if(RoomList[y].getTech())
        {
            cout << "Yes" << endl;
        }
        else
        {
            cout << "No" << endl;
        }
        cout << "Description: " << RoomList[y].getDescription() << endl;
        cout << endl << "How this room compares to your event: " << endl;
        // This is the display variable that was updated in the grading process
        cout << RoomList[y].getDisplay() << endl;
    }


    cout << "Please type the name of the room you would like to book or CANCEL to cancel booking: ";
    string BookingRoom;
    getline(cin, BookingRoom);

    if(BookingRoom == "CANCEL")
    {
        // kill the program
        return 0;
    }

    ofstream outfile;
    // NOTE TO DR MATHIAS: May need to change this line of code. It did not work when we did not have the full
    // location of the text file.

    // open the email.txt file
    outfile.open("/Users/Alex/College/Fall 2017/Git Repo for Alg Project/alg-project/email.txt", std::ofstream::out | std::ofstream::trunc);
    // add the "email" to be sent
    outfile << UserName << " from " << UserClub << " requests the following room: " << BookingRoom << endl;
    outfile << "Event Description: " << UserEvent.getDescription() << endl;
    outfile << "Contact " << UserName << " at: " << UserEmail;


    return 0;
}