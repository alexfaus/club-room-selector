# Analysis of Algorithms Project - Club Room Selector

This project is by Alexander Faus, Jess Moser, and Kara Konieczny.

## The roomlist.txt File

This file is a list of rooms we sampled from Florida Southern College campus. The format for the text file is as follows:

```
Name of Room
Dates available separated by spaces and in the format MM/DD/YYYY
Capacity
Number of Tables
Whiteboard (Y/N)
Technology (Y/N)
Description of Room
ENDROOM
```

The file also ends with
```
ENDFILE
```


### What the Program Does

The program follows a series of questioning the user for input about themselves and the club event. Then, the program will iterate through the rooms from the roomlist.txt file and assign each room a grade based on how close it is to the user�s event explanation. It then displays the rooms in best to worst order and prompts the user to choose a room by typing its name. Finally, the email.txt file is modified to create a message to request the room.


```
// Sample email.txt file after running program:

USER NAME from CLUB NAME requests the following room: NAME OF ROOM FROM roomlist.txt
Event Description: DESCRIPTION OF EVENT
Contact USER NAME at: USER@EMAIL.COM
```

### IMPORTANT!
The text files did not work without the complete address of the text file.

```
// Change the following lines of code
ifstream in("/Users/Alex/College/Fall 2017/Git Repo for Alg Project/alg-project/roomlist.txt");
outfile.open("/Users/Alex/College/Fall 2017/Git Repo for Alg Project/alg-project/email.txt", std::ofstream::out | std::ofstream::trunc);
```